import React, { Component } from 'react';
import UserService from '../services/UserService';


export default class UserList extends Component {

    constructor(props) {
        super(props)
        this.userService = new UserService()
    }

    state = {
        users: []
    }


    componentWillMount = () => {
        this.getUsers()
    }

    getUsers = () => {
        this.userService.fetchAll().then(response => {
            response.json().then(result => {
                this.setState({ users: result })
            })
        })
    }

    render() {
        return (
            <div style={{ width: "70%", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: 80 }}>
                <h2>Lista de usuários</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <th>
                                Nome
                             </th>
                            <th>
                                Email
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.users.map(user => (
                            <tr key={user.id}>
                                <td>
                                    {user.name}
                                </td>
                                <td>
                                    {user.email}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

        )
    }

}



