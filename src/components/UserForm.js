import React, { Component } from 'react';
import UserService from '../services/UserService';


export default class UserForm extends Component {

    constructor(props) {
        super(props)
        this.userService = new UserService()
    }

    state = {
        user: {

        }
    }

    render() {
        return (
            <div style={{ backgroundColor: "rgb(247, 247, 247)" }}>
                <div style={{ width: "50%", display: "block", marginLeft: "auto", marginRight: "auto", backgroundColor: "rgb(247, 247, 247)", padding: "2%" }}>
                    <h2>Formulário de usuários</h2>
                    <form>
                        <div className="form-group">
                            <label htmlFor="name">Nome</label>
                            <input type="text" className="form-control" id="name" aria-describedby="Nome" placeholder="Digite seu email" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email" className="form-control" id="email" aria-describedby="email" placeholder="Digite seu email" />
                        </div>
                        <button type="button" className="btn btn-primary">Salvar</button>
                    </form>
                </div>
            </div>
        )
    }

}



